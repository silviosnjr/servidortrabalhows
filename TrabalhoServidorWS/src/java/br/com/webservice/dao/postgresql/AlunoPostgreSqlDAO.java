/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webservice.dao.postgresql;

import br.com.webservice.dao.AlunoDAO;
import br.com.webservice.utils.Conexao;
import br.com.webservice.vo.Aluno;
import br.com.webservice.vo.Endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author especialist
 */
public class AlunoPostgreSqlDAO implements AlunoDAO{

    @Override
    public List<Aluno> getAlunos() throws SQLException{
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        Aluno aluno;
        
        List<Aluno> listaAluno = new ArrayList<>();
        String qryAlunos = "select * from aluno order by matricula";
       
        try{
            con = Conexao.conectar();
            pstm = con.prepareStatement(qryAlunos);
            rs = pstm.executeQuery();
            while(rs.next()){
                aluno = new Aluno();

                aluno.setMatricula( rs.getInt("matricula") );
                aluno.setCpf( rs.getString("cpf") );
                aluno.setNome( rs.getString("nome") );
                aluno.setIdade( rs.getInt("idade") );

                aluno.setEndereco( getEndereco(aluno.getMatricula()) );
                listaAluno.add(aluno);                
            }
        } finally{
            if (pstm != null)
                pstm.close();
            if (rs != null)
                rs.close();
            Conexao.closeConnection(con);
        }
        return listaAluno;
    }
    
    @Override
    public List<Endereco> getEndereco(int matricula) throws SQLException{
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        List<Endereco> listaEndereco = new ArrayList<>();
        Endereco endereco;
        
        String qryEndereco = "select e.id, e.logradouro, e.numero, e.complemento, e.cep, e.bairro, e.cidade, e.estado "
                + "from endereco e where e.matricula = ?";
        try{
            con = Conexao.conectar();
            pstm = con.prepareStatement(qryEndereco);
            pstm.setInt(1, matricula);
            rs = pstm.executeQuery();
            while(rs.next()){
                endereco = new Endereco();
                endereco.setId( rs.getInt("id") );
                endereco.setLogradouro( rs.getString("logradouro") );
                endereco.setNumero( rs.getString("numero") );
                endereco.setComplemento( rs.getString("complemento") );
                endereco.setCep( rs.getInt("cep") );
                endereco.setBairro( rs.getString("bairro") );
                endereco.setCidade( rs.getString("cidade") );
                endereco.setEstado( rs.getString("estado") );

                listaEndereco.add(endereco);
            }
        } finally{
            if (pstm != null)
                pstm.close();
            if (rs != null)
                rs.close();
            Conexao.closeConnection(con);
        }
        
        return listaEndereco;
        
    }

    @Override
    public Aluno buscaAluno(int matricula) throws SQLException{
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        Aluno aluno = null;
        String qryBuscaAluno = "select * from aluno where matricula = ?";
       
        try{
            con = Conexao.conectar();
            pstm = con.prepareStatement(qryBuscaAluno);
            pstm.setInt(1, matricula);
            rs = pstm.executeQuery();
            if(rs.next()){
                aluno = new Aluno();

                aluno.setMatricula( rs.getInt("matricula") );
                aluno.setCpf( rs.getString("cpf") );
                aluno.setNome( rs.getString("nome") );
                aluno.setIdade( rs.getInt("idade") );

                aluno.setEndereco( getEndereco(aluno.getMatricula()) );                                
            }
        } finally{
            if (pstm != null)
                pstm.close();
            if (rs != null)
                rs.close();
            Conexao.closeConnection(con);
        }
        return aluno;
    }

    /*
    * 
    */
    @Override
    public Integer salvar(Aluno aluno) throws SQLException{
        Connection con = null;
        PreparedStatement pstm = null;
        String qryInsertAluno = "insert into aluno (nome, cpf, idade) values (?,?,?)";
        Integer result = null;
        Integer chaveGerada = 0;
        try{    
            con = Conexao.conectar();
            con.setAutoCommit(false);
            pstm = con.prepareStatement(qryInsertAluno, PreparedStatement.RETURN_GENERATED_KEYS);            
            pstm.setString(1, aluno.getNome());
            pstm.setString(2, aluno.getCpf());
            pstm.setInt(3, aluno.getIdade());
            pstm.executeUpdate();
            ResultSet rs = pstm.getGeneratedKeys(); 
            if(rs.next())
                chaveGerada = rs.getInt(1);
            if(chaveGerada > 0){
                List<Endereco> listaEndereco = aluno.getEndereco();
                for(Endereco endereco : listaEndereco){
                    endereco.setMatricula(chaveGerada);
                    salvarEndereco(endereco, con);
                }  
            }
            con.commit();
            result = 1;
        } finally{
            if (pstm != null)
                pstm.close();
            Conexao.closeConnection(con);
        }
        return result;
    }

    @Override
    public Integer apagar(int matricula) throws SQLException{
        Connection con = null;
        PreparedStatement pstm = null;
        String qryDeleteAluno = "delete from aluno where matricula = ?";
        Integer result = null;
        try{
            con = Conexao.conectar();
            pstm = con.prepareStatement(qryDeleteAluno);
            pstm.setInt(1, matricula);
            result = pstm.executeUpdate();            
        } finally{
            if (pstm != null)
                pstm.close();
            Conexao.closeConnection(con);
        }
        return result;
    }
    
    @Override
    public Integer apagarEndereco(int id) throws SQLException{
        Connection con = null;
        PreparedStatement pstm = null;
        String qryDeleteEndereco = "delete from endereco where id = ?";
        Integer result = null;
        try{
            con = Conexao.conectar();
            pstm = con.prepareStatement(qryDeleteEndereco);
            pstm.setInt(1, id);
            result = pstm.executeUpdate();            
        } finally{
            if (pstm != null)
                pstm.close();
            Conexao.closeConnection(con);
        }
        return result;
    }

    @Override
    public Integer alterar(Aluno aluno) throws SQLException{
        Connection con = null;
        PreparedStatement pstm = null;
        String qryUpdateAluno = "update aluno set nome = ?, cpf = ?, idade = ? "
                + "where matricula = ?";
        Integer result = null;
        try{
            con = Conexao.conectar();
            con.setAutoCommit(false);
            pstm = con.prepareStatement(qryUpdateAluno);
            pstm.setString(1, aluno.getNome());
            pstm.setString(2, aluno.getCpf());
            pstm.setInt(3, aluno.getIdade());
            pstm.setInt(4, aluno.getMatricula());
            result = pstm.executeUpdate(); 
            /* Se recebeu resposta positiva na alteração do aluno
            ** então faz o update dos endereços   
            */
            if(result == 1){
                for(Endereco endereco : aluno.getEndereco()){
                    endereco.setMatricula(aluno.getMatricula());
                    if(endereco.getId() > 0)
                        alterarEndereco(endereco, con);
                    else                        
                        salvarEndereco(endereco, con);                    
                } 
            }                
            con.commit();
        } finally{
            if (pstm != null)
                pstm.close();
            Conexao.closeConnection(con);
        }
        return result;
    }

    @Override
    public Integer salvarEndereco(Endereco endereco, Connection con) throws SQLException {
        PreparedStatement pstm = null;
        String qryInsertEndereco = "insert into endereco "
                + "(matricula, logradouro, numero, complemento, bairro, "
                + "cep, cidade, estado) values (?,?,?,?,?,?,?,?)";
        Integer result = null;
        try{
            pstm = con.prepareStatement(qryInsertEndereco);
            pstm.setInt(1, endereco.getMatricula());
            pstm.setString(2, endereco.getLogradouro());
            pstm.setString(3, endereco.getNumero());
            pstm.setString(4, endereco.getComplemento());
            pstm.setString(5, endereco.getBairro());
            if(endereco.getCep() == 0)
                pstm.setNull(6, Types.NULL);
            else
                pstm.setInt(6, endereco.getCep());
            pstm.setString(7, endereco.getCidade());
            pstm.setString(8, endereco.getEstado());
            result = pstm.executeUpdate();
            
        } finally{
            if (pstm != null)
                pstm.close();
        }
        return result;
    }

    @Override
    public Integer alterarEndereco(Endereco endereco, Connection con) throws SQLException {
        PreparedStatement pstm = null;
        String qryUpdateEndereco = "update endereco set logradouro = ?, numero = ?, "
                + "complemento = ?, bairro = ?, cep = ?, cidade = ?, estado = ? "
                + "where matricula = ? and id = ?";
        Integer result = null;
        try{
            pstm = con.prepareStatement(qryUpdateEndereco);            
            pstm.setString(1, endereco.getLogradouro());
            pstm.setString(2, endereco.getNumero());
            pstm.setString(3, endereco.getComplemento());
            pstm.setString(4, endereco.getBairro());
            pstm.setInt(5, endereco.getCep());
            pstm.setString(6, endereco.getCidade());
            pstm.setString(7, endereco.getEstado());
            pstm.setInt(8, endereco.getMatricula());
            pstm.setInt(9, endereco.getId());
            result = pstm.executeUpdate();            
        } finally{
            if (pstm != null)
                pstm.close();
        }
        return result;
    }

}
