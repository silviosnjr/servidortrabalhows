/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webservice.dao;

import br.com.webservice.vo.Aluno;
import br.com.webservice.vo.Endereco;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author especialist
 */
public interface AlunoDAO {
    
    List<Aluno> getAlunos() throws SQLException;
    List<Endereco> getEndereco(int matricula) throws SQLException;
    Aluno buscaAluno(int matricula) throws SQLException;
    Integer salvar(Aluno aluno) throws SQLException;
    Integer salvarEndereco(Endereco endereco, Connection con) throws SQLException;
    Integer apagar(int matricula) throws SQLException;
    Integer apagarEndereco(int id) throws SQLException;
    Integer alterar(Aluno aluno) throws SQLException;
    Integer alterarEndereco(Endereco endereco, Connection con) throws SQLException;
    
}
