/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webservice.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author especialist
 */
public class Conexao {
    
    public static Connection con; 

    public static boolean status = false; 	
    static String urlLocal = "jdbc:postgresql://localhost:5432/trabalhows";  
    static String userLocal = "postgres";  
    static String passLocal = "root"; 
    
    public static Connection conectar() {
        try {
            Class.forName("org.postgresql.Driver");                       
            con = DriverManager.getConnection(urlLocal, userLocal, passLocal);            
            status = true;                  
        } catch (ClassNotFoundException | SQLException e) {        	
            status = false;
        }
        return con;
    }
	
    /**
     * Fecha a conexao passada via parametro
     * @param conn - A Connection a ser fechada.
     * @throws SQLException 
     */
    public static void closeConnection(Connection conn) throws SQLException{ 		
        conn.close();		
    }

    public static boolean getStatus() { 
        return status; 
    }
    
}
