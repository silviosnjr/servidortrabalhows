/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webservice.resource;

import br.com.webservice.dao.AlunoDAO;
import br.com.webservice.dao.postgresql.AlunoPostgreSqlDAO;
import br.com.webservice.vo.Aluno;
import java.sql.SQLException;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author ricardo.souza1
 */
@Path("aluno")
public class AlunoResource {

    @Context
    private UriInfo context;
    AlunoDAO alunoDao = new AlunoPostgreSqlDAO();

    public AlunoResource() {
    }
    
    @GET
    @Path("/{id}") 
    @Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
    public Aluno getAluno(@PathParam("id") int matricula) {        
        Aluno aluno = null;        
        try{
            aluno = alunoDao.buscaAluno(matricula);
        } catch(SQLException e){
            System.out.println(e);
        }            
        return aluno;        
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
    public Response getAlunos() {        
        List<Aluno> listaAluno = null;
        
        try{
            listaAluno = alunoDao.getAlunos();
        } catch(SQLException e){
            System.out.println(e);
        }    
        
        GenericEntity<List<Aluno>> resultListaAluno = 
                new GenericEntity<List<Aluno>>(listaAluno){};
        
        return Response.ok().entity(resultListaAluno).build();
        
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON+";charset=UTF-8")
    public String addALuno(Aluno aluno) {
        
        String result = null;
        
        try{
            result = String.valueOf(alunoDao.salvar(aluno));
        } catch(SQLException e){
            System.out.println(e);
        } 
        return result;
    }
    
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON+";charset=UTF-8")
    public String editALuno(Aluno aluno) {
        String result = null;
        try{
            result = String.valueOf(alunoDao.alterar(aluno));
        } catch(SQLException e){
            System.out.println(e);
        } 
        return result;
    }
    
    @DELETE
    @Path("/{matricula}") 
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteALuno(@PathParam("matricula") int matricula) {
        String result = null;        
        try{
            result = String.valueOf(alunoDao.apagar(matricula));
        } catch(SQLException e){
            System.out.println(e);
        } 
        return result;
    }
    
    @DELETE
    @Path("endereco/{id}") 
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteEndereco(@PathParam("id") int id) {
        String result = null;        
        try{
            result = String.valueOf(alunoDao.apagarEndereco(id));
        } catch(SQLException e){
            System.out.println(e);
        } 
        return result;
    }
    
    
}
